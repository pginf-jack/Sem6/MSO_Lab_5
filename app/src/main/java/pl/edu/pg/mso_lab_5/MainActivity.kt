package pl.edu.pg.mso_lab_5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import pl.edu.pg.mso_lab_5.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Example of a call to a native method
        binding.sampleText.text = stringFromJNI()

        var orig: IntArray = intArrayOf(1, 2, 3, 1, 2, 4, 5, 2, 5);
        binding.originalArrayText.text = Arrays.toString(orig);
        var modified = withoutDuplicates(orig);
        binding.modifiedArrayText.text = modified.contentToString();
    }

    /**
     * A native method that is implemented by the 'mso_lab_5' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String
    external fun withoutDuplicates(input: IntArray): IntArray

    companion object {
        // Used to load the 'mso_lab_5' library on application startup.
        init {
            System.loadLibrary("mso_lab_5")
        }
    }
}