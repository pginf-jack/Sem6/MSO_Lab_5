#include <jni.h>
#include <string>
#include <vector>

extern "C" JNIEXPORT jstring JNICALL
Java_pl_edu_pg_mso_1lab_15_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jintArray JNICALL
Java_pl_edu_pg_mso_1lab_15_MainActivity_withoutDuplicates(
        JNIEnv* env,
        jobject instance,
        jintArray input) {
    std::string hello = "Hello from C++";
    std::vector<jint> filtered{};

    jint *ptr = env->GetIntArrayElements(input, NULL);

    jint len = env->GetArrayLength(input);
    bool addMe = false;
    for (int i = 0; i < len; i++) {
        jint current = *(ptr + i);
        addMe = true;
        for (int i = 0; i < filtered.size(); i++) {
            if (current == filtered[i]) {
                addMe = false;
                break;
            }
        }
        if (addMe) {
            filtered.push_back(current);
        }
    }

    jintArray result = env->NewIntArray(filtered.size());
    env->SetIntArrayRegion(result, 0, filtered.size(), &filtered[0]);

    return result;
}
